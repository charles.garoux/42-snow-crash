# 42-snow-crash

**Description** :
    Projet introductif à la sécurité en informatique, Snow Crash a pour but de faire
découvrirla sécurité dans différents sous-domaines,avec une approche orientée développeur. <br>
    Nous devons nous familiariser avec plusieurs langages (ASM/perl/php..),
développer une certaine logique pour comprendre des programmes inconnus, et ainsi prendre
conscience des problèmes liés à de simples erreurs de programmation.

**Support** : ISO de machine virtuel